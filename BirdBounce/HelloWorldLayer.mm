#import "HelloWorldLayer.h"
#import "Bird.h"

@implementation HelloWorldLayer

+ (id)scene {
    
    CCScene *scene = [CCScene node];
    HelloWorldLayer *layer = [HelloWorldLayer node];
    [scene addChild:layer];
    return scene;
    
}

- (id)init {
    
    if((self = [super initWithColor:ccc4(104, 104, 104, 255)])) {
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        [self setTouchEnabled:YES];
        [[[CCDirector sharedDirector] view] setMultipleTouchEnabled:YES];
        
        CCSprite *bg = [CCSprite spriteWithFile:@"bgV_sky.png"];
        [bg setPosition:ccp(160, 284)];
        bg.rotation = 90;
        
        CCSprite *sun = [CCSprite spriteWithFile:@"bgV_sun.png"];
        [sun setPosition:ccp(200, 100)];
        [self addChild:sun z:1];
        
        CCSprite *landscape = [CCSprite spriteWithFile:@"bgV_hills.png"];
        [landscape setPosition:ccp(278, 74)];
        [self addChild:landscape z:2];
        
        CCSprite *nest1 = [CCSprite spriteWithFile:@"nest1.png"];
        [nest1 setPosition:ccp(winSize.width - 40, 80)];
        [self addChild:nest1 z:5];
        
        CCSprite *nest2 = [CCSprite spriteWithFile:@"nest2.png"];
        [nest2 setPosition:ccp(40, 40)];
        [self addChild:nest2 z:5];
        
        CCSprite *nest3 = [CCSprite spriteWithFile:@"nest3.png"];
        [nest3 setPosition:ccp(winSize.width - 60, 180)];
        [self addChild:nest3 z:5];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"bird_green.plist"];
        
        NSMutableArray *flutterAnimFrames = [NSMutableArray array];
        for(int i = 1; i <= 2; ++i) {
            [flutterAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"Bird%d.png", i]]];
        }
        
        CCSprite *bird3 = [CCSprite spriteWithSpriteFrameName:@"Bird1.png"];;
        [bird3 setPosition:ccp(winSize.width - 55, 210)];
        [self addChild:bird3 z:4];
        
        CCSprite *nest4 = [CCSprite spriteWithFile:@"nest4.png"];
        [nest4 setPosition:ccp(60, 200)];
        [self addChild:nest4 z:5];
        
        CCSprite *bird4 = [CCSprite spriteWithSpriteFrameName:@"Bird1.png"];;
        [bird4 setPosition:ccp(78, 235)];
        [self addChild:bird4 z:4];
        
        // Create a world
        b2Vec2 gravity = b2Vec2(0.0f, 0.0f);
        _world = new b2World(gravity);
        
        // Create edges around the entire screen
        b2BodyDef groundBodyDef;
        groundBodyDef.position.Set(0,0);
        _groundBody = _world->CreateBody(&groundBodyDef);
        
        b2EdgeShape groundBox;
        b2FixtureDef groundBoxDef;
        groundBoxDef.shape = &groundBox;
        
        groundBox.Set(b2Vec2(0,0), b2Vec2(winSize.width/PTM_RATIO, 0));
        _bottomFixture = _groundBody->CreateFixture(&groundBoxDef);
        
        groundBox.Set(b2Vec2(0,0), b2Vec2(0, winSize.height/PTM_RATIO));
        _groundBody->CreateFixture(&groundBoxDef);
        
        groundBox.Set(b2Vec2(0, winSize.height/PTM_RATIO), b2Vec2(winSize.width/PTM_RATIO,
                                                                  winSize.height/PTM_RATIO));
        _groundBody->CreateFixture(&groundBoxDef);
        
        groundBox.Set(b2Vec2(winSize.width/PTM_RATIO, winSize.height/PTM_RATIO),
                      b2Vec2(winSize.width/PTM_RATIO, 0));
        _groundBody->CreateFixture(&groundBoxDef);
        
        // Create sprite and add it to the layer
        CCSprite *ball = [[Bird alloc] initWithBirdColor:@"green"];
        ball.position = ccp(240, 300);
        ball.tag = 1;
        [self addChild:ball z:3];
        
        // Create ball body
        b2BodyDef ballBodyDef;
        ballBodyDef.type = b2_dynamicBody;
        ballBodyDef.position.Set(240/PTM_RATIO, 300/PTM_RATIO);
        ballBodyDef.userData = ball;
        b2Body * ballBody = _world->CreateBody(&ballBodyDef);
        
        // Create circle shape
        b2CircleShape circle;
        circle.m_radius = 26.0/PTM_RATIO;
        
        // Create shape definition and add to body
        b2FixtureDef ballShapeDef;
        ballShapeDef.shape = &circle;
        ballShapeDef.density = 1.0f;
        ballShapeDef.friction = 0.f;
        ballShapeDef.restitution = 1.0f;
        _ballFixture = ballBody->CreateFixture(&ballShapeDef);
        
        b2Vec2 force = b2Vec2(10, 10);
        ballBody->ApplyLinearImpulse(force, ballBodyDef.position);
        
        [self schedule:@selector(tick:)];
        
        self.touchEnabled = YES;
    }
    return self;
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSSet *allTouches = [event allTouches];
    
    if([allTouches count] == 2){
        UITouch * touch1 = [[allTouches allObjects] objectAtIndex:0];
        CGPoint location1 = [touch1 locationInView: [touch1 view]];
        location1 = [[CCDirector sharedDirector] convertToGL:location1];
        
        UITouch * touch2 = [[allTouches allObjects] objectAtIndex:1];
        CGPoint location2 = [touch2 locationInView: [touch2 view]];
        location2 = [[CCDirector sharedDirector] convertToGL:location2];
        
        int touch1X = location1.x;
        int touch2X = location2.x;
        
        int touch1Y = location1.y;
        int touch2Y = location2.y;
        
        //FIND THE LEFT-MOST TOUCH
        int minX = (touch1X < touch2X) ? touch1X : touch2X;
        int maxX = (minX == touch1X) ? touch2X : touch1X;
        
        //FIND THE BOTTOM-MOST TOUCH
        int minY = (touch1Y < touch2Y) ? touch1Y : touch2Y;
        int maxY = (minY == touch1Y) ? touch2Y : touch1Y;
        
        int touchXDiff = maxX - minX;
        int touchYDiff = maxY - minY;
        
        int offDiffX = touch1X - touch2X;
        int offDiffY = touch1Y - touch2Y;
        
        float angleRadians = atanf((float)offDiffY / (float)offDiffX);
        float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);
        float cloudAngle = -1 * angleDegrees;
        
        int cloudX = touchXDiff/2 + minX;
        int cloudY = touchYDiff/2 + minY;
        
        // Create paddle and add it to the layer
        // if not already on the layer
        if (_paddle == nil) {
            _paddle = [CCSprite spriteWithFile:@"cloud.png"];
            [self addChild:_paddle z:3];
        }
        _paddle.position = ccp(cloudX, cloudY);
        [_paddle setRotation:cloudAngle];
        
        // Create paddle body
        paddleBodyDef.type = b2_dynamicBody;
        paddleBodyDef.position.Set(cloudX/PTM_RATIO, cloudY/PTM_RATIO);
        paddleBodyDef.userData = _paddle;
        _paddleBody = _world->CreateBody(&paddleBodyDef);
        //_paddleBody->SetTransform(_paddleBody->GetPosition(), angleDegrees);
        
        // Create paddle shape
        paddleShape.SetAsBox(_paddle.contentSize.width/PTM_RATIO/2,
                             _paddle.contentSize.height/PTM_RATIO/2);
        
        // Create shape definition and add to body
        paddleShapeDef.shape = &paddleShape;
        paddleShapeDef.density = 10.0f;
        paddleShapeDef.friction = 0.4f;
        paddleShapeDef.restitution = 0.1f;
        _paddleFixture = _paddleBody->CreateFixture(&paddleShapeDef);
    }
    else if(NO){
        if (_mouseJoint != NULL) return;
        
        // Restrict paddle along the x axis
        b2PrismaticJointDef jointDef;
        b2Vec2 worldAxis(1.0f, 0.0f);
        jointDef.collideConnected = true;
        jointDef.Initialize(_paddleBody, _groundBody,
                            _paddleBody->GetWorldCenter(), worldAxis);
        _world->CreateJoint(&jointDef);
        
        UITouch *myTouch = [touches anyObject];
        CGPoint location = [myTouch locationInView:[myTouch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        
        if (_paddleFixture->TestPoint(locationWorld)) {
            b2MouseJointDef md;
            md.bodyA = _groundBody;
            md.bodyB = _paddleBody;
            md.target = locationWorld;
            md.collideConnected = true;
            md.maxForce = 1000.0f * _paddleBody->GetMass();
            
            _mouseJoint = (b2MouseJoint *)_world->CreateJoint(&md);
            _paddleBody->SetAwake(true);
        }
    }
    
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    NSSet *allTouches = [event allTouches];
    BOOL testingMove = NO;
    
    if(testingMove && [allTouches count] == 2){
        UITouch * touch1 = [[allTouches allObjects] objectAtIndex:0];
        CGPoint location1 = [touch1 locationInView: [touch1 view]];
        location1 = [[CCDirector sharedDirector] convertToGL:location1];
        
        UITouch * touch2 = [[allTouches allObjects] objectAtIndex:1];
        CGPoint location2 = [touch2 locationInView: [touch2 view]];
        location2 = [[CCDirector sharedDirector] convertToGL:location2];
        
        int touch1X = location1.x;
        int touch2X = location2.x;
        
        int touch1Y = location1.y;
        int touch2Y = location2.y;
        
        //FIND THE LEFT-MOST TOUCH
        int minX = (touch1X < touch2X) ? touch1X : touch2X;
        int maxX = (minX == touch1X) ? touch2X : touch1X;
        
        //FIND THE BOTTOM-MOST TOUCH
        int minY = (touch1Y < touch2Y) ? touch1Y : touch2Y;
        int maxY = (minY == touch1Y) ? touch2Y : touch1Y;
        
        int touchXDiff = maxX - minX;
        int touchYDiff = maxY - minY;
        
        int offDiffX = touch1X - touch2X;
        int offDiffY = touch1Y - touch2Y;
        
        float angleRadians = atanf((float)offDiffY / (float)offDiffX);
        float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);
        float cloudAngle = -1 * angleDegrees;
        
        int cloudX = touchXDiff/2 + minX;
        int cloudY = touchYDiff/2 + minY;
        
        _paddle.position = ccp(cloudX, cloudY);
        [_paddle setRotation:cloudAngle];
        
        // Create paddle body
        paddleBodyDef.type = b2_dynamicBody;
        paddleBodyDef.position.Set(cloudX/PTM_RATIO, cloudY/PTM_RATIO);
        paddleBodyDef.userData = _paddle;
        _paddleBody = _world->CreateBody(&paddleBodyDef);
        //_paddleBody->SetTransform(_paddleBody->GetPosition(), angleDegrees);
        
        // Create paddle shape
        paddleShape.SetAsBox(_paddle.contentSize.width/PTM_RATIO/2,
                             _paddle.contentSize.height/PTM_RATIO/2);
        
        // Create shape definition and add to body
        paddleShapeDef.shape = &paddleShape;
        paddleShapeDef.density = 10.0f;
        paddleShapeDef.friction = 0.4f;
        paddleShapeDef.restitution = 0.1f;
        _paddleFixture = _paddleBody->CreateFixture(&paddleShapeDef);
    }
    else{
        if (_mouseJoint == NULL) return;
        
        UITouch *myTouch = [touches anyObject];
        CGPoint location = [myTouch locationInView:[myTouch view]];
        location = [[CCDirector sharedDirector] convertToGL:location];
        b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
        
        _mouseJoint->SetTarget(locationWorld);
    }
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        _world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        _world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
}

- (void)tick:(ccTime) dt {
    _world->Step(dt, 10, 10);
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            CCSprite *sprite = (CCSprite *)b->GetUserData();
            
            // if ball is going too fast, turn on damping
            if (sprite.tag == 1) {
                static int maxSpeed = 10;
                
                b2Vec2 velocity = b->GetLinearVelocity();
                float32 speed = velocity.Length();
                
                if (speed > maxSpeed) {
                    b->SetLinearDamping(0.5);
                } else if (speed < maxSpeed) {
                    b->SetLinearDamping(0.0);
                }
                
            }
            if(sprite == _paddle){
                b2Vec2 b2Position = b2Vec2(sprite.position.x/PTM_RATIO, sprite.position.y/PTM_RATIO);
                float32 b2Angle = -1 * CC_DEGREES_TO_RADIANS(sprite.rotation);
                b->SetTransform(b2Position, b2Angle);
            }
            else{
                sprite.position = ccp(b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
                sprite.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
            }
            
        }
    }
    
}

- (void)dealloc {
    
    delete _world;
    _groundBody = NULL;
    [super dealloc];
    
}

@end