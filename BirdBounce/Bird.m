//
//  Bird.m
//  AniBird
//
//  Created by Daniel Burke on 1/24/13.
//  Copyright 2013 Daniel Burke. All rights reserved.
//

#import "Bird.h"


@implementation Bird

- (id)initWithBirdColor:(NSString *)color{
    if((self = [super init])){
        if([color isEqualToString:@"green"] || [color isEqualToString:@"red"]){
            _birdColor = color;
        }
        else{
            _birdColor = @"green";
        }
    }
    
    NSString *birdPlistName = [NSString stringWithFormat:@"bird_%@.plist", _birdColor];
    NSString *birdImageName = [NSString stringWithFormat:@"bird_%@.png", _birdColor];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: birdPlistName];
    _spriteSheet = [CCSpriteBatchNode batchNodeWithFile:birdImageName];
    [self addChild:_spriteSheet z:3];
    
    NSMutableArray *flutterAnimFrames = [NSMutableArray array];
    for(int i = 1; i <= 2; ++i) {
        [flutterAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"Bird%d.png", i]]];
    }
    
    CCAnimation *flutterAnim = [CCAnimation animationWithFrames:flutterAnimFrames delay:0.1f];
    
    _bird = [CCSprite spriteWithSpriteFrameName:@"Bird1.png"];
    _flutterAction = [CCRepeatForever actionWithAction:
                          [CCAnimate actionWithAnimation:flutterAnim restoreOriginalFrame:NO]];
    
    [_bird runAction:_flutterAction];
    [_flutterAction retain];
    [_spriteSheet addChild:_bird];
    
    return self;
}

-(void)flutterLeft{
        [_bird stopAction:_flutterAction];
    
        NSMutableArray *flutterLeftAnimFrames = [NSMutableArray array];
        for(int i = 7; i <= 8; ++i) {
            [flutterLeftAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"Bird%d.png", i]]];
        }

        CCAnimation *flutterAnim = [CCAnimation animationWithFrames:flutterLeftAnimFrames delay:0.1f];
    
        _flutterAction = [CCRepeatForever actionWithAction:
                              [CCAnimate actionWithAnimation:flutterAnim restoreOriginalFrame:NO]];
    
        [_bird runAction:_flutterAction];
        [_flutterAction retain];
}

-(void)flutterRight{
    [_bird stopAction:_flutterAction];
    
    NSMutableArray *flutterLeftAnimFrames = [NSMutableArray array];
    for(int i = 5; i <= 6; ++i) {
        [flutterLeftAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"Bird%d.png", i]]];
    }
    
    CCAnimation *flutterAnim = [CCAnimation animationWithFrames:flutterLeftAnimFrames delay:0.1f];
    
    _flutterAction = [CCRepeatForever actionWithAction:
                      [CCAnimate actionWithAnimation:flutterAnim restoreOriginalFrame:NO]];
    
    [_bird runAction:_flutterAction];
    [_flutterAction retain];
}


-(void)flutter{
    [_bird stopAction:_flutterAction];
    
    NSMutableArray *flutterLeftAnimFrames = [NSMutableArray array];
    for(int i = 1; i <= 2; ++i) {
        [flutterLeftAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"Bird%d.png", i]]];
    }
    
    CCAnimation *flutterAnim = [CCAnimation animationWithFrames:flutterLeftAnimFrames delay:0.1f];
    
    _flutterAction = [CCRepeatForever actionWithAction:
                      [CCAnimate actionWithAnimation:flutterAnim restoreOriginalFrame:NO]];
    
    [_bird runAction:_flutterAction];
    [_flutterAction retain];
}

-(void)bounce:(float)cloudAngle{
    cloudAngle = -1 * cloudAngle;
    CGFloat bounceAngle = cloudAngle - 90;
}

@end
