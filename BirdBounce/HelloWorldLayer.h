//
//  HelloWorldLayer.h
//  BirdBreakOut
//
//  Created by Daniel Burke on 2/3/13.
//  Copyright Daniel Burke 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "Bird.h"

#define PTM_RATIO 32.0

@interface HelloWorldLayer : CCLayerColor {
    b2World *_world;
    b2Body *_groundBody;
    b2Fixture *_bottomFixture;
    b2Fixture *_ballFixture;
    
    b2Body *_paddleBody;
    b2Fixture *_paddleFixture;
    
    b2MouseJoint *_mouseJoint;
    CCSprite *_paddle;
    
    b2PolygonShape paddleShape;
    b2BodyDef paddleBodyDef;
    b2FixtureDef paddleShapeDef;
    
}

+ (id)scene;

@end
